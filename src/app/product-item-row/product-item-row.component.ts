import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../instances/product.instance';

@Component({
  selector: 'app-product-item-row',
  templateUrl: './product-item-row.component.html',
  styleUrls: ['./product-item-row.component.scss']
})
export class ProductItemRowComponent {
  @Input() product: Product;
  @Output() removeClicked = new EventEmitter();

  onRemoveClicked() {
    this.removeClicked.emit(this.product);
  }
}
