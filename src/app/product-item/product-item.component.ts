import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../instances/product.instance';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent {
  @Input() product: Product;
  @Output() buyClicked = new EventEmitter();

  onBuyClicked() {
    this.buyClicked.emit(this.product);
  }
}
