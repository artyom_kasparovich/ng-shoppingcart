import { BagProduct } from './product.instance';

interface ITax {
  id: string;
  value: number;
  label: string;
  total: null | number;
}

export class Order {
  taxes: ITax[] = [{
    id: 'GST',
    value: 0.05,
    label: 'GST (5%)',
    total: null
  }];

  items: BagProduct[] = [];

  constructor(items: BagProduct[]) {
    this.items = items;
  }

  getSubtotal(): number {
    return this.items.reduce((acc, o) => acc + (o.count * o.price), 0);
  }

  getTaxes(): ITax[] {
    const subTotal = this.getSubtotal();

    return this.taxes.map((o) => ({
      ...o,
      total: subTotal * o.value
    }));
  }

  getTaxesSum(): number {
    return this.getTaxes().reduce((acc, o) => acc + o.total, 0);
  }

  getTotal(): number {
    return this.getSubtotal() + this.getTaxesSum();
  }
}
