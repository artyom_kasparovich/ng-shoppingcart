export class Product {
  id: string;
  name: string;
  price: number;
  quantity: number;
  img: string;
  color: string;
}

export class BagProduct extends Product {
  count: number;
}
