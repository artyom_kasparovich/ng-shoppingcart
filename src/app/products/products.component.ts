import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from '../services/product.service';
import { BagService } from '../services/bag.service';
import { MessagesService } from '../services/messages.service';
import { Product } from '../instances/product.instance';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  title = 'Products';
  size = 6;
  page = 1;

  products: Product[];

  constructor(
    private productService: ProductService,
    private bagService: BagService,
    private spinner: NgxSpinnerService,
    private messageService: MessagesService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.getProducts();
  }

  onBuyClicked(product: Product): void {
    this.bagService.addItem(product);
    this.messageService.add(`${product.name} was added to bag`);
  }

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(products => {
        this.products = products;
        this.spinner.hide();
      });
  }
}
