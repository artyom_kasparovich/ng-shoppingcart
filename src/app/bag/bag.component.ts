import { Component } from '@angular/core';
import { BagService } from '../services/bag.service';
import { MessagesService } from '../services/messages.service';
import { Product, BagProduct } from '../instances/product.instance';
import { Order } from '../instances/order.instance';

@Component({
  selector: 'app-bag',
  templateUrl: './bag.component.html',
  styleUrls: ['./bag.component.scss']
})
export class BagComponent {
  title = 'My Bag';

  constructor(
    private bagService: BagService,
    private messageService: MessagesService
  ) {}

  getItems(): BagProduct[] {
    return this.bagService.getItems();
  }

  getOrder(): Order {
    return new Order(this.getItems());
  }

  onRemoveClicked(product: Product): void {
    this.bagService.removeItem(product);
    this.messageService.remove(product.id);
  }
}
