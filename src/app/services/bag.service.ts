import { Injectable } from '@angular/core';
import { BagProduct, Product } from '../instances/product.instance';

@Injectable({
  providedIn: 'root'
})
export class BagService {
  private items: BagProduct[] = [];

  getItems(): BagProduct[] {
    return this.items;
  }

  addItem(item: Product): void {
    const items = this.getItems();
    const founded = items.find((o) => o.id === item.id);

    if (!founded) {
      items.push({...item, count: 1});
    } else {
      founded.count = founded.count + 1;
    }
  }

  removeItem(item: Product): void {
    const items = this.getItems();
    const founded = items.find((o) => o.id === item.id);

    if (founded.count === 1) {
      this.items = items.filter((o) => o.id !== item.id);
    } else {
      founded.count = founded.count - 1;
    }
  }
}
