import { Injectable } from '@angular/core';

interface IMessage {
  id: string;
  text: string;
  type: string;
}

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messages: IMessage[] = [];

  getMessages(): IMessage[] {
    return this.messages;
  }

  add(message: string): void {
    this.messages.push({
      id: `id_${Math.random().toString(16).slice(2)}`,
      text: message,
      type: 'info'
    });
  }

  remove(id: string): void {
    this.messages = this.messages.filter((m) => m.id !== id);
  }
}
