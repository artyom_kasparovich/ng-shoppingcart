import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Product } from '../instances/product.instance';

const scheme = (product): Product => {
  return {
    id: product.productId,
    name: product.productName,
    price: product.productPrice,
    quantity: product.quantity,
    img: product.thumbnails.b2,
    color: product.productColor
  };
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: Product[];

  constructor(
    private http: HttpClient
  ) {}

  private url = 'https://api.myjson.com/bins/1g2o7w';

  getProducts (): Observable<Product[]> {
    return this.http.get<any>(this.url)
      .pipe(
        pluck('results'),
        map((products: Product[]) => products.map(scheme))
      );
  }
}
